import {
  isWorkingDay, hourInMS, getDayOfWeek, addTimeToDate, convertWorkingHoursToNormalHours, getHour, nextWorkingDate, isWorkingHour,
} from './utils'

function CalculateDueDate(date, turnaroundHours) {
  let newDate = date
  if (!isWorkingDay(getDayOfWeek(date)) || !isWorkingHour(getHour(date))) newDate = nextWorkingDate(date)
  return addTimeToDate(newDate, convertWorkingHoursToNormalHours(turnaroundHours, newDate) * hourInMS)
}

export { CalculateDueDate as default }
