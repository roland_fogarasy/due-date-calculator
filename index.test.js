import CalculateDueDate from './index'

test('Calculate due dates with valid input', () => {
  expect(CalculateDueDate(new Date('1970-01-01 09:00:00+0000'), 15).toUTCString()).toBe(new Date('1970-01-02 16:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('1970-01-01 09:00:00+0000'), 0).toUTCString()).toBe(new Date('1970-01-01 09:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('1970-01-01 15:00:00+0000'), 3).toUTCString()).toBe(new Date('1970-01-02 10:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('1970-01-02 09:00:00+0000'), 8).toUTCString()).toBe(new Date('1970-01-05 09:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('1970-01-02 09:00:00+0000'), 17).toUTCString()).toBe(new Date('1970-01-06 10:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('1970-01-02 09:00:00+0000'), 18).toUTCString()).toBe(new Date('1970-01-06 11:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('1970-01-02 09:00:00+0000'), 5 * 8 * 4 + 10).toUTCString()).toBe(new Date('1970-02-02 11:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('2020-12-17 09:00:00+0000'), 8).toUTCString()).toBe(new Date('2020-12-18 09:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('2020-12-18 16:00:00+0000'), 39).toUTCString()).toBe(new Date('2020-12-25 15:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('2020-12-18 16:00:00+0000'), 40).toUTCString()).toBe(new Date('2020-12-25 16:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('2020-12-18 16:00:00+0000'), 46).toUTCString()).toBe(new Date('2020-12-28 14:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('2020-12-18 09:00:00+0000'), 49).toUTCString()).toBe(new Date('2020-12-28 10:00:00+0000').toUTCString())
})

test('Calculate due dates with invalid input', () => {
  // 12-19 is Saturday
  expect(CalculateDueDate(new Date('2020-12-19 09:00:00+0000'), 7).toUTCString()).toBe(new Date('2020-12-21 16:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('2020-12-19 09:00:00+0000'), 8).toUTCString()).toBe(new Date('2020-12-22 09:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('2020-12-19 09:00:00+0000'), 9).toUTCString()).toBe(new Date('2020-12-22 10:00:00+0000').toUTCString())
  // 12-20 is sunday
  expect(CalculateDueDate(new Date('2020-12-20 09:00:00+0000'), 47).toUTCString()).toBe(new Date('2020-12-28 16:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('2020-12-20 09:00:00+0000'), 48).toUTCString()).toBe(new Date('2020-12-29 09:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('2020-12-20 09:00:00+0000'), 49).toUTCString()).toBe(new Date('2020-12-29 10:00:00+0000').toUTCString())

  expect(CalculateDueDate(new Date('2020-12-18 17:00:00+0000'), 1).toUTCString()).toBe(new Date('2020-12-21 10:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('1970-01-01 00:00:00+0000'), 0).toUTCString()).toBe(new Date('1970-01-01 09:00:00+0000').toUTCString())
  expect(CalculateDueDate(new Date('1970-01-01 00:00:00+0000'), 2).toUTCString()).toBe(new Date('1970-01-01 11:00:00+0000').toUTCString())
})
