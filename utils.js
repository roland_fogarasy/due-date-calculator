const hourInMS = 60 * 60 * 1000
const workingHoursStart = 9
const workingHoursDuration = 8
const workingHoursEnd = workingHoursStart + workingHoursDuration

function isWorkingDay(dayOfWeek) {
  if (!Number.isInteger(dayOfWeek) || dayOfWeek < 0 || dayOfWeek > 6) throw Error('Input must be between 0 and 6!')
  return dayOfWeek <= 4
}

function isWeekDay(dayOfWeek) {
  return !isWorkingDay(dayOfWeek)
}

function getDayOfWeek(date) {
  // Convert US style to European (Monday is the first day of the week)
  // @returns {number} 0-6, Monday-Saturday
  return (6 + new Date(date).getDay()) % 7
}

function isWorkingHour(hour) {
  if (!Number.isInteger(hour) || hour < 0 || hour > 23) throw Error('Input must be between 0 and 23!')
  return hour >= workingHoursStart && hour < workingHoursEnd
}

function getHour(date) {
  return (date.getTime() % (24 * hourInMS)) / hourInMS
}

function convertWorkingHoursToNormalHours(workingHour, startDate = 0) {
  const days = Math.max(Math.floor((getHour(new Date(startDate)) - workingHoursStart + workingHour) / workingHoursDuration), 0) // ?
  const weekends = Math.floor((getDayOfWeek(startDate) + days) / 5) // ?
  return days * (24 - workingHoursDuration) + weekends * 48 + workingHour
}

function convertDateToMS(date) {
  return new Date(date).getTime()
}
function addTimeToDate(date, timeMS) {
  return new Date(convertDateToMS(date) + timeMS)
}

function nextWorkingDate(date) {
  let nextDate = addTimeToDate(date, 24 * hourInMS)
  if (getHour(date) < 9) nextDate = date
  if (getDayOfWeek(date) === 5) nextDate = addTimeToDate(date, 48 * hourInMS)
  nextDate = addTimeToDate(nextDate, getHour(new Date(date)) * hourInMS * -1 + workingHoursStart * hourInMS)
  return new Date(nextDate)
}

export {
  isWorkingDay, isWeekDay, isWorkingHour, hourInMS, workingHoursEnd, getHour, convertWorkingHoursToNormalHours, convertDateToMS, addTimeToDate, getDayOfWeek, workingHoursStart, nextWorkingDate,
}
