import {isWorkingDay, isWeekDay, isWorkingHour, hourInMS, workingHoursEnd, getHour, convertWorkingHoursToNormalHours, convertDateToMS, addTimeToDate, getDayOfWeek, workingHoursStart, nextWorkingDate,} from './utils'

test('isWorkingDay', () => {
  expect(isWorkingDay(0)).toBe(true)
  expect(isWorkingDay(1)).toBe(true)
  expect(isWorkingDay(2)).toBe(true)
  expect(isWorkingDay(3)).toBe(true)
  expect(isWorkingDay(4)).toBe(true)
  expect(isWorkingDay(5)).toBe(false)
  expect(isWorkingDay(6)).toBe(false)
})

test('isWorkingDay with invalid argument', () => {
  expect(() => isWorkingDay(-1)).toThrowError()
  expect(() => isWorkingDay(12)).toThrowError()
  expect(() => isWorkingDay(false)).toThrowError()
})

test('convertWorkingHoursToNormalHours', () => {
  expect(convertWorkingHoursToNormalHours(0)).toBe(0)
  expect(convertWorkingHoursToNormalHours(8)).toBe(8)
  expect(convertWorkingHoursToNormalHours(15)).toBe(15)
  expect(convertWorkingHoursToNormalHours(16)).toBe(16)
  expect(convertWorkingHoursToNormalHours(17)).toBe(33)

  expect(convertWorkingHoursToNormalHours(39)).toBe(135)
  expect(convertWorkingHoursToNormalHours(40)).toBe(136)

})
